// pages/new/new.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    new_book:[]
  },
  // 跳详情
  detail() {
    wx.navigateTo({
      url: '../detail/detail',
    })
  },
  // 立即阅读 待替换
  read() {
    wx.navigateTo({
      url: '../book/book',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '新书推荐',
    })
    // console.log(options)
    var _this = this
    _this.setData({ new_book: JSON.parse(options.new_book) })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})