Page({

  /**
   * 页面的初始数据
   */
  data: {
    hot:[]
  },
  // 跳详情
  detail() {
    wx.navigateTo({
      url: '../detail/detail',
    })
  },
  // 立即阅读看小说 待替换
  read() {
    wx.navigateTo({
      url: '../book/book',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this=this
    wx: wx.setNavigationBarTitle({
      title: '热门小说',
    })
    // wx.request({
    //   url: 'https://www.laldd.com/App/popular_novels',
    //   success: function (data) {
    //     console.log(data.data)
    //     _this.setData({ hot: data.data })
    //   }
    // })
    var hot_book=JSON.parse(options.hot_book)
    _this.setData({hot:hot_book})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})