// pages/short/short.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    new_short:[],
    hot_short:[]
  },
  // 跳详情
  detail() {
    wx.navigateTo({
      url: '../detail/detail',
    })
  },
  // 立即阅读 待替换
  read() {
    wx.navigateTo({
      url: '../book/book',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx: wx.setNavigationBarTitle({
      title: '短篇小说',
    })
    var _this = this;
    // 三条数据
    var short = JSON.parse(options.short_book)
    var new_short=short.splice(0,3)
    _this.setData({ new_short: new_short }) 
    _this.setData({ hot_short: short}) 
    // 取完三条数据之后
    // _this.setData({ hot_short: JSON.parse(options.short_book) })
    console.log(_this.data.new_short)
    console.log(_this.data.hot_short)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var _this = this;
    _this.setData({ hot_short: _this.data.hot_short })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})