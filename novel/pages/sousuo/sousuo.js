Page({

  /**
   * 页面的初始数据
   */
  data: {
    ischange:false,
    iscancel:true,
    isdisplay:false,
    searching:[],
    val:'',
    searchlist:[],
    searchlistlength:' '
  },
  // 返回
  back(){
    wx.navigateBack({
      delta: 1
    })
  },
  // 取消和搜索切换 
  change(e){
    // console.log(e.detail.value)
    if (e.detail.value){
      this.setData({
        ischange:true,
        iscancel:false
      })
    }
  },
// 搜索 获取文本框的值
  getvalue(e){
    this.setData({ val: e.detail.value })
    // console.log(e)
    // console.log(this.data.val)
    
  },
  // 搜索
  search(){
    var _this = this;
    wx.request({
      url: 'https://www.laldd.com/App/search_box/?search_message=' + _this.data.val,
      success: function (data) {
        console.log(data.data)
        _this.setData({ 
          searchlist:data.data,
          searchlistlength: data.data.length
          })
        // console.log(_this.data.val)
        // console.log(data.data.length)
      }
    })
    this.setData({
      isdisplay:true
    })
  },
  del() {
    console.log(111)
    this.setData({
      val: ''
    })
    // console.log(this.data.val)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this=this;
    wx.setNavigationBarTitle({
      title: '搜索',
    })
    wx.request({
      url: 'https://www.laldd.com/App/recommend',
      success: function (data) {
        _this.setData({ searching: data.data })
        
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})