// pages/mall/mall.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bookId:'',
    imgUrls: [],
    editor: [],
    hot_book:[],
    hot_count:[],
    short_count:[],
    short_book:[],
    free_count:[],
    sell_count:[],
    sell_book:[],
    like_count:[],
    new_count1: [],
    new_count2:[],
    new_book:[],
    countDownDay: 0,
    countDownHour: 0,
    countDownMinute: 0,
    countDownSecond: 0,
    countDown:true,
    countDownTime: 20,
    indicatorDots: true,
    autoplay: true,
    circular:true,
    interval: 3000,
    duration: 1000,
    // indicator-color:#f00,
    active:'#FA6A0D'
  },
  changeIndicatorDots(e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay(e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange(e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange(e) {
    this.setData({
      duration: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    wx.setNavigationBarTitle({
      title: '书城',
    })
    // 轮播图
    wx.request({
      url: 'https://www.laldd.com/App/home_rotation_chart/',
      success:function(data){
        // console.log(data)
        _this.setData({ imgUrls:data.data})
      }
    })
    // 主编推荐
    wx.request({
      url: 'https://www.laldd.com/App/recommend',
      success:function(data){
        _this.setData({editor:data.data})
      //  console.log(_this.data.editor)
      }
    })
    // 热门小说???Unexpected end of JSON input
    wx.request({
      url: 'https://www.laldd.com/App/time_free',
      success: function (data) {
        // console.log(data.data)
        // console.log(data.data.slice(0, 3))
        _this.setData({ hot_book:data.data})
        _this.setData({hot_count:data.data.slice(0, 3)})

      }
    })
    // 短篇小说
    wx.request({
      url: 'https://www.laldd.com/App/short_story',
      success: function (data) {
        // console.log(data.data)
        // console.log(data.data.slice(0, 3))
        _this.setData({ short_book: data.data })
        _this.setData({ short_count: data.data.slice(0, 3) })
      }
    })
    // 新书推荐 ??替换
    wx.request({
      url: 'https://www.laldd.com/App/recommend',
      success: function (data) {
        // console.log(data.data)
        _this.setData({ new_count1: data.data.slice(0, 3) })
        _this.setData({ new_count2: data.data.slice(3, 5) })
        _this.setData({ new_book: data.data})
        // console.log("下",_this.data.new_count2)
      }
    })
    // wx.request({
    //   url: 'https://www.laldd.com/App/time_free',
    //   success: function (data) {
    //     // console.log(data.data)
    //     _this.setData({free_count:data.data.slice(0,3)})
    //   }
    // })
    // 猜你喜欢
    wx.request({
      url: 'https://www.laldd.com/App/recommend',
      success: function (data) {
        // console.log(data.data)
        _this.setData({ like_count: data.data.slice(0, 6)})
      }
    })
    // 畅销书单?? 原接口数据不全
    wx.request({
      url: 'https://www.laldd.com/App/short_story',
      success: function (data) {
        // console.log("限时免费",data.data)
        _this.setData({ sell_count: data.data.slice(0, 6) })
        _this.setData({sell_book:data.data})
      }
    })

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var _this = this;
    // 限时免费
    // if (_this.data.countDown) {
      wx.request({
        url: 'https://www.laldd.com/App/recommend',
        success: function (data) {
          // console.log(data.data)
          _this.setData({ free_count: data.data.slice(0, 3) })
        }
      })
      // 限时免费 倒计时  三天
      // var totalSecond = 259200;
      var totalSecond = _this.data.countDownTime;  // 测试
      var interval = setInterval(function () {
        this.setData({ countDown: false })
        // 秒数
        var second = totalSecond;
        // 天数位
        var day = Math.floor(second / 3600 / 24);
        var dayStr = day.toString();
        if (dayStr.length == 1) dayStr = '0' + dayStr;
        // 小时位
        var hr = Math.floor((second - day * 3600 * 24) / 3600);
        var hrStr = hr.toString();
        if (hrStr.length == 1) hrStr = '0' + hrStr;
        // 分钟位
        var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
        var minStr = min.toString();
        if (minStr.length == 1) minStr = '0' + minStr;
        // 秒位
        var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
        var secStr = sec.toString();
        if (secStr.length == 1) secStr = '0' + secStr;
        this.setData({
          countDownDay: dayStr,
          countDownHour: hrStr,
          countDownMinute: minStr,
          countDownSecond: secStr,
        });
        totalSecond--;
        // console.log(this.data.countDown)
        if (totalSecond < 0) {
          // clearInterval(interval);
          // wx.showToast({
          //   title: '活动已结束',
          // });
          // 倒计时结束后重新请求数据
          totalSecond=this.data.countDownTime;
          this.setData({
            countDown: true
          });
          wx.request({
            url: 'https://www.laldd.com/App/recommend',
            success: function (data) {
              // console.log(data.data)
              _this.setData({ free_count: data.data.slice(3, 6) })
            }
          })
        }
      }.bind(this), 1000);
    },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 热门小说更多
  hot_more() {
    var _this = this;
    wx.navigateTo({
      url: '../hot/hot?hot_book=' + JSON.stringify(_this.data.hot_book),
    })
  },
  // 短篇小说更多
  short_more() {
    var _this = this;
    wx.navigateTo({
      url: '../short/short?short_book=' + JSON.stringify(_this.data.short_book),
    })
  },
  // 短篇小说更多
  sell_more() {
    var _this = this;
    wx.navigateTo({
      url: '../sell/sell?sell_book=' + JSON.stringify(_this.data.sell_book),
    })
  },
  // 猜你喜欢 换一批
  batch(){
    var _this=this
    wx.request({
      url: 'https://www.laldd.com/App/recommend',
      success: function (data) {
        // console.log(data.data)
        _this.setData({ like_count: data.data.slice(0, 6) })
      }
    })
  },
  // 新书推荐更多
  new_more() {
    var _this = this;
    wx.navigateTo({
      url: '../new/new?new_book=' + JSON.stringify(_this.data.new_book),
    })
  },
  // 搜索
  sousuo(){
    wx.navigateTo({
      url: '../sousuo/sousuo',
    })
  },
  // 跳详情
  // detail() {
  //   var _this=this;
  //   console.log(_this.data)
  //   wx.navigateTo({
  //     url: '../detail/detail',
  //   })
  // },
  // 立即阅读 待替换
  read() {
    wx.navigateTo({
      url: '../book/book',
    })
  },
  // 分类
  sort(){
    wx.navigateTo({
      url: '../sort/sort',
    })
  }
})

